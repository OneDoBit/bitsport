export const state = () => ({
  desktopMenu: [
    {
      title: 'Games',
      submenus: [
        {
          icon: 'pvp.png',
          title: 'PVP',
          link: '/pvp',
          notifications: 1,
          submenuLinks: [
            {
              title: 'FIFA 2022',
              link: '/pvp/fifa'
            },
            {
              title: 'Mortal Kombat X',
              link: '/pvp/mortal-kombat-x'
            }
          ]
        },
        {
          icon: 'live.svg',
          title: 'Live',
          link: '/live',
          notifications: 5,
          submenuLinks: [
            {
              title: 'FIFA 2022',
              link: '/live/fifa'
            },
            {
              title: 'Mortal Kombat X',
              link: '/live/mortal-kombat-x'
            }
          ]
        },
        {
          icon: 'esports.svg',
          title: 'eSports',
          link: '/esports',
          notifications: 0,
          submenuLinks: [
            {
              title: 'FIFA 2022',
              link: '/esports/fifa'
            },
            {
              title: 'Mortal Kombat X',
              link: '/esports/mortal-kombat-x'
            }
          ]
        }
      ]
    },
    {
      title: 'Account',
      submenus: [
        {
          icon: 'stakes.svg',
          title: 'My stackes',
          link: { url:'/my-stackes', type: 'internal'},
          notifications: 1
        },
        {
          icon: 'settings.svg',
          title: 'Account settings',
          link: { url:'/account-settings', type: 'internal'},
          notifications: 0
        }
      ]
    },
    {
      title: 'Support',
      submenus: [
        {
          icon: 'chat.svg',
          title: 'Support Chat',
          link: { url:'https://nedobitkov.ca', type: 'external'},
          notifications: 0
        }
      ]
    }
  ],
  mobileMenu: [
    {
      icon: 'pvp.png',
      title: 'PVP',
      link: { url:'/pvp', type: 'internal'},
      notifications: 1
    },
    {
      icon: 'live.svg',
      title: 'Live',
      link: { url:'/live', type: 'internal'},
      notifications: 2
    },
    {
      icon: 'esports.svg',
      title: 'eSports',
      link: { url:'/esports', type: 'internal'},
      notifications: 11
    },
    {
      icon: 'settings.svg',
      title: 'Account',
      link: { url:'/account', type: 'internal'},
      notifications: 0
    },
    {
      icon: 'chat.svg',
      title: 'Support',
      link: { url:'https://nedobitkov.ca', type: 'external'},
    }
  ]
})
export const actions = {
}
export const mutations = {
}
export const getters = {
}
  