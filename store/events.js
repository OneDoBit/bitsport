export const state = () => ({
  dashboard: {
    live: {
      events: [
        {
          id: 1,
          isLive: true,
          time: 12,
          team_one: {
            name: 'Team 1',
            score: 1,
            stake: 1.5
          },
          team_two: {
            name: 'Team 2',
            score: 2,
            stake: 1
          },
          link: 'https://nedobitkov.com'
        },
        {
          id: 1,
          isLive: true,
          time: 12,
          team_one: {
            name: 'Team 1',
            score: 1,
            stake: 1.5
          },
          team_two: {
            name: 'Team 2',
            score: 2,
            stake: 1
          },
          link: 'https://nedobitkov.com'
        },
        {
          id: 1,
          isLive: true,
          time: 12,
          team_one: {
            name: 'Team 1',
            score: 1,
            stake: 1.5
          },
          team_two: {
            name: 'Team 2',
            score: 2,
            stake: 1
          },
          link: 'https://nedobitkov.com'
        },
        {
          id: 1,
          isLive: true,
          time: 12,
          team_one: {
            name: 'Team 1',
            score: 1,
            stake: 1.5
          },
          team_two: {
            name: 'Team 2',
            score: 2,
            stake: 1
          },
          link: 'https://nedobitkov.com'
        },
        {
          id: 1,
          isLive: true,
          time: 12,
          team_one: {
            name: 'Team 1',
            score: 1,
            stake: 1.5
          },
          team_two: {
            name: 'Team 2',
            score: 2,
            stake: 1
          },
          link: 'https://nedobitkov.com'
        }
      ]
    },
    pvp: {
      events: [
        {
          id: 1,
          player: 'Castro_12',
          prize: {
            amount: 10,
            currency: "USDT"
          },
          fee: {
            amount: 5,
            currency: "USDT"
          },
          link: 'https://nedobitkov.com'
        }
      ]
    }
  },
  esports: {
    events: [
      {
        id: 1,
        isLive: true,
        time: 12,
        team_one: {
          name: 'Team 1',
          score: 1,
          stake: 1.5
        },
        team_two: {
          name: 'Team 2',
          score: 2,
          stake: 1
        },
        link: 'https://nedobitkov.com'
      },
      {
        id: 2,
        isLive: true,
        time: 12,
        team_one: {
          name: 'Team 1',
          score: 1,
          stake: 1.5
        },
        team_two: {
          name: 'Team 2',
          score: 2,
          stake: 1
        },
        link: 'https://nedobitkov.com'
      }
    ],
  },
  live: {
    events: [
      {
        id: 1,
        isLive: true,
        time: 12,
        team_one: {
          name: 'Team 1',
          score: 1,
          stake: 1.5
        },
        team_two: {
          name: 'Team 2',
          score: 2,
          stake: 1
        },
        link: 'https://nedobitkov.com'
      }
    ],
  },
  pvp: {
    events: [
      {
        id: 1,
        player: 'Castro_12',
        prize: {
          amount: 10,
          currency: "USDT"
        },
        fee: {
          amount: 5,
          currency: "USDT"
        },
        link: 'https://nedobitkov.com'
      },
      {
        id: 2,
        player: 'Castro_11',
        prize: {
          amount: 1,
          currency: "USDT"
        },
        fee: {
          amount: 50,
          currency: "USDT"
        },
        link: 'https://nedobitkov.com'
      }
    ]
  }
})
export const actions = {
}
export const mutations = {
}
export const getters = {
}
  