export const state = () => ({
  username: '',
  password: '',
  email: '',
  name: '',

  accountBalance: {
    amount: 0.00,
    currency: 'USDT'
  },

  stakes: {
    events: [
      {
        id: 1,
        event: 'PVP with Castro',
        time: '18:47',
        odds: '1.18',
        stake: {
          amount: 10,
          currency: 'USDT'
        },
        result: {
          decision: 'win',
          amount: 10,
          currency: 'USDT'
        }
      },
      {
        id: 2,
        event: 'PVP with Castro',
        time: '18:47',
        odds: '1.18',
        stake: {
          amount: 10,
          currency: 'USDT'
        },
        result: {
          decision: 'lose',
          amount: 10,
          currency: 'USDT'
        }
      }
    ]
  },

  pvp: {
    // isActive, waiting, isDispute // true or false
    activeEvents: [
      {
        isActive: true,
        id: 1,
        player: 'Castro_1',
        prize: {
          amount: 18,
          currency: "USDT"
        },
        fee: {
          amount: 10,
          currency: "USDT"
        },
        link: 'https://nedobitkov.com'
      },
      {
        waiting: true,
        id: 2,
        player: 'Castro_2',
        prize: {
          amount: 18,
          currency: "USDT"
        },
        fee: {
          amount: 10,
          currency: "USDT"
        },
        link: 'https://nedobitkov.com'
      },
    ],

    disputesEvents: [
      {
        isDispute: true,
        id: 1,
        player: 'Castro_12',
        prize: {
          amount: 10,
          currency: "USDT"
        },
        fee: {
          amount: 5,
          currency: "USDT"
        },
        link: 'https://nedobitkov.com'
      },
    ]
  },

  live: {
    eventsActive: [
      {
        active: {
          stakeTeam: 'Team 1'
        },
        id: 1,
        isLive: true,
        time: 12,
        team_one: {
          name: 'Team 1',
          score: 1,
          stake: 1.5
        },
        team_two: {
          name: 'Team 2',
          score: 2,
          stake: 1
        },
        link: 'https://nedobitkov.com'
      }
    ]
  },

  esports: {
    eventsActive: [
      {
        active: {
          stakeTeam: 'Team 1'
        },
        id: 1,
        isLive: true,
        time: 12,
        team_one: {
          name: 'Team 1',
          score: 1,
          stake: 1.5
        },
        team_two: {
          name: 'Team 2',
          score: 2,
          stake: 1
        },
        link: 'https://nedobitkov.com'
      }
    ],
  }
})
export const actions = {
}
export const mutations = {
  USER_UPDATE(state, payload) {
    state.password = payload.password;
    state.email = payload.email;
  },
  // Prototype
  USER_ACCOUNT_BALANCE(state, payload) {
    state.accountBalance.amount += parseFloat(payload.amount);
    state.accountBalance.currency = payload.currency
  },
  USER_CREATE_MATCH(state, payload) {
    state.pvp.activeEvents.push(payload);
  },
  USER_CREATE_STAKE(state, {payload, item}) {
    state[item].eventsActive.push(payload);
  },
  // STORE_DATE_TOKEN(state, payload) {
  //   state.tokenStats.length = payload;
  // }
}
export const getters = {
}
  